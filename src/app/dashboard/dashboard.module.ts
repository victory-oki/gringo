import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { DashboardRoutingModule } from "./dashboard-routing.module";
import { LayoutComponent } from "./layout/layout.component";
import { MyCardsComponent } from "./my-cards/my-cards.component";
import { MoneyToMobileComponent } from "./money-to-mobile/money-to-mobile.component";
import { MoneyToPersonComponent } from "./money-to-person/money-to-person.component";
import { MoneyBoxComponent } from "./money-box/money-box.component";
import { TransactionsComponent } from "./transactions/transactions.component";
import { AnalyticsComponent } from "./analytics/analytics.component";
import { NgxMatIntlTelInputModule } from "ngx-mat-intl-tel-input";
import { MatFormFieldModule } from "@angular/material/form-field";
import { SharedModule } from "../shared/shared.module";

@NgModule({
  declarations: [
    LayoutComponent,
    MyCardsComponent,
    MoneyToMobileComponent,
    MoneyToPersonComponent,
    MoneyBoxComponent,
    TransactionsComponent,
    AnalyticsComponent,
  ],
  imports: [
    CommonModule,
    DashboardRoutingModule,
    NgxMatIntlTelInputModule,
    MatFormFieldModule,
    SharedModule,
  ],
})
export class DashboardModule {}
