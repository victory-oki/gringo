import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { environment } from "src/environments/environment";
import { Observable } from "rxjs";
@Injectable({
  providedIn: "root",
})
export class DashboardService {
  constructor(private httpClient: HttpClient) {}

  getMyCards(): Observable<any> {
    const url = `${environment.api_base_url}my_cards`;
    return this.httpClient.get(url);
  }
  getBeneficiaries(): Observable<any> {
    const url = `${environment.api_base_url}beneficiaries`;
    return this.httpClient.get(url);
  }
  getTransactions(): Observable<any> {
    const url = `${environment.api_base_url}transactions`;
    return this.httpClient.get(url);
  }
  getAnalytics(): Observable<any> {
    const url = `${environment.api_base_url}analytics`;
    return this.httpClient.get(url);
  }
  getMoneyBox(): Observable<any> {
    const url = `${environment.api_base_url}money_box`;
    return this.httpClient.get(url);
  }
}
