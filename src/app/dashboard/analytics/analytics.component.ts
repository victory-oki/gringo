import { Component, OnInit, OnDestroy } from "@angular/core";
import Chart from "chart.js";
import { DashboardService } from "../services/dashboard/dashboard.service";
import { IAnalytics } from "../models/dashboard.interface";
import { Subscription } from "rxjs";

@Component({
  selector: "app-analytics",
  templateUrl: "./analytics.component.html",
  styleUrls: ["./analytics.component.scss"],
})
export class AnalyticsComponent implements OnInit, OnDestroy {
  analytics: IAnalytics;
  private subscription: Subscription;
  isLoading = true;
  constructor(private dashboardService: DashboardService) {}

  ngOnInit(): void {
    this.getAnalytics();
  }

  getAnalytics() {
    this.subscription = this.dashboardService.getAnalytics().subscribe(
      (data) => {
        console.log(data);
        this.isLoading = false;
        this.analytics = { ...data };
        this.initializeChart();
      },
      (error) => {
        this.isLoading = false;
        console.log(error);
      }
    );
  }

  initializeChart() {
    var ctx = document.getElementById("myChart");
    Chart.defaults.global.defaultFontColor = "#878b97";
    Chart.defaults.global.defaultFontFamily = "'Poppins', sans-serif";
    var chart = new Chart(ctx, {
      // The type of chart we want to create
      type: "line",

      // The data for our dataset
      data: {
        labels: ["Jan 20", "Jan 21", "Jan 22", "Jan 23", "Jan 24", "Jan 25"],
        datasets: [
          {
            label: "Income",
            borderColor: "#2d98ff",
            backgroundColor: "transparent",
            pointBorderWidth: 6,
            pointRadius: 8,
            pointBorderColor: "#fff",
            pointBackgroundColor: "#2d98ff",
            hoverBorderColor: "#a54bff5b",
            hoverBackgroundColor: "#a54bff",
            pointHoverBorderWidth: 6,
            pointHoverRadius: 8,
            data: [...this.analytics.income],
          },
          {
            label: "Expense",
            borderColor: "#e5e5e7",
            backgroundColor: "transparent",
            data: [...this.analytics.expenses],
          },
        ],
      },

      // Configuration options go here
      options: {
        responsive: true,
        maintainAspectRatio: false,
        legend: {
          display: false,
        },
        tooltips: {
          xPadding: 20,
          yPadding: 20,
          cornerRadius: 15,
          backgroundColor: "#fff",
          titleFontFamily: "'Poppins', sans-serif",
          titleFontColor: "#878b97",
          titleSpacing: 10,
          bodyFontFamily: "'Poppins', sans-serif",
          bodyFontColor: "#0b122e",
          bodySpacing: 20,
          bodyFontSize: 16,
          bodyFontStyle: "500",
        },
      },
    });
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
}
