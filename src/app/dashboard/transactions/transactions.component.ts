import { Component, OnInit, OnDestroy } from "@angular/core";
import { DashboardService } from "../services/dashboard/dashboard.service";
import { ITransaction } from "../models/dashboard.interface";
import { Subscription } from "rxjs";

@Component({
  selector: "app-transactions",
  templateUrl: "./transactions.component.html",
  styleUrls: ["./transactions.component.scss"],
})
export class TransactionsComponent implements OnInit, OnDestroy {
  transactions: ITransaction[];
  private subscription: Subscription;
  isLoading = true;

  constructor(private dashboardService: DashboardService) {}

  ngOnInit(): void {
    this.getTransactions();
  }

  getTransactions() {
    this.subscription = this.dashboardService.getTransactions().subscribe(
      (data) => {
        console.log(data);
        this.isLoading = false;
        this.transactions = [...data];
      },
      (error) => {
        this.isLoading = false;
        console.log(error);
      }
    );
  }
  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
}
