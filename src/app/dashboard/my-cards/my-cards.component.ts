import { Component, OnInit, OnDestroy } from "@angular/core";
import { DashboardService } from "../services/dashboard/dashboard.service";
import { IMyCards } from "../models/dashboard.interface";
import { Subscription } from "rxjs";

@Component({
  selector: "app-my-cards",
  templateUrl: "./my-cards.component.html",
  styleUrls: ["./my-cards.component.scss"],
})
export class MyCardsComponent implements OnInit, OnDestroy {
  myCard: IMyCards;
  private subscription: Subscription;
  isLoading = true;
  constructor(private dashboardService: DashboardService) {}

  ngOnInit(): void {
    this.getMyCardsData();
  }

  getMyCardsData() {
    this.subscription = this.dashboardService.getMyCards().subscribe(
      (data) => {
        this.isLoading = false;
        console.log(data);
        this.myCard = { ...data };
      },
      (error) => {
        this.isLoading = false;
        console.log(error);
      }
    );
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
}
