import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MoneyToPersonComponent } from './money-to-person.component';

describe('MoneyToPersonComponent', () => {
  let component: MoneyToPersonComponent;
  let fixture: ComponentFixture<MoneyToPersonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MoneyToPersonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MoneyToPersonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
