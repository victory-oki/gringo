import { Component, OnInit, OnDestroy } from "@angular/core";
import { DashboardService } from "../services/dashboard/dashboard.service";
import { IBeneficiaries } from "../models/dashboard.interface";
import { Subscription } from "rxjs";

@Component({
  selector: "app-money-to-person",
  templateUrl: "./money-to-person.component.html",
  styleUrls: ["./money-to-person.component.scss"],
})
export class MoneyToPersonComponent implements OnInit, OnDestroy {
  beneficiaries: IBeneficiaries;
  private subscription: Subscription;
  isLoading = true;
  constructor(private dashboardService: DashboardService) {}

  ngOnInit(): void {
    this.getBeneciaries();
  }

  getBeneciaries() {
    this.subscription = this.dashboardService.getBeneficiaries().subscribe(
      (data) => {
        console.log(data);
        this.isLoading = false;
        this.beneficiaries = { ...data };
      },
      (error) => {
        this.isLoading = false;
        console.log(error);
      }
    );
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
}
