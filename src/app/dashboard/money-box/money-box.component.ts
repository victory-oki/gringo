import { Component, OnInit, OnDestroy } from "@angular/core";
import { DashboardService } from "../services/dashboard/dashboard.service";
import { IMoneyBox } from "../models/dashboard.interface";
import { Subscription } from "rxjs";

@Component({
  selector: "app-money-box",
  templateUrl: "./money-box.component.html",
  styleUrls: ["./money-box.component.scss"],
})
export class MoneyBoxComponent implements OnInit, OnDestroy {
  width = "0%";
  progress: number;
  private subscription: Subscription;
  isLoading = true;
  constructor(private dashboardService: DashboardService) {}

  ngOnInit(): void {
    this.getMoneyBox();
  }

  getMoneyBox() {
    this.subscription = this.dashboardService.getMoneyBox().subscribe(
      ({ progress }) => {
        this.progress = progress;
        this.isLoading = false;
        this.width = `${this.progress}%`;
      },
      (error) => {
        this.isLoading = false;
        console.log(error);
      }
    );
  }
  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
}
