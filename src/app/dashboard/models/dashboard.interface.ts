export interface IMyCards {
  account_number: string;
  payment_card_balance: number;
  status: string;
  expires: number;
  card_name: string;
  card_type: string;
  credit_card_balance: number;
}

export interface IBeneficiaries {
  images: string[];
  account_numbers: string[];
}

export interface ITransaction {
  name: string;
  date: string;
  price: number;
  icon_name: string;
  icon_color: string;
}

export interface IAnalytics {
  income: number[];
  expenses: number[];
}

export interface IMoneyBox {
  progress: number;
}
