import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { SharedRoutingModule } from "./shared-routing.module";
import { GlobalLoaderComponent } from "./component/global-loader/global-loader.component";
import { MatProgressSpinnerModule } from "@angular/material/progress-spinner";

@NgModule({
  declarations: [GlobalLoaderComponent],
  imports: [CommonModule, SharedRoutingModule, MatProgressSpinnerModule],
  exports: [GlobalLoaderComponent],
})
export class SharedModule {}
