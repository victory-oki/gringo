import { Component, OnInit, Input } from "@angular/core";

@Component({
  selector: "app-global-loader",
  templateUrl: "./global-loader.component.html",
  styleUrls: ["./global-loader.component.scss"],
})
export class GlobalLoaderComponent implements OnInit {
  @Input() diameter: number = 80;
  constructor() {}

  ngOnInit(): void {}
}
